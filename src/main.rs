extern crate rodio;
extern crate date_component;
use dell_api;
use snipeit_rust;
use std::io;
use std::io::BufReader;
use chrono::{Duration, Datelike, Local, Utc, DateTime};

#[derive(Clone, Debug)]
struct Command {
    action: Action,
    data: String
}

#[derive(PartialEq, Clone, Debug)]
enum Action {
    Close,
    Audit,
    Report,
    New,
    None
}

impl Command {
    fn none() -> Command {
        Command {
            action: Action::None,
            data: "0".to_string()
        }
    }
}

fn main() {
    let client = snipeit_rust::SnipeItClient::new("http://172.16.6.21",
                                                  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
    let mut last_command = Command::none();
    loop {
        let current_scan = read_command(get_input());
        if current_scan.action != Action::None {
            last_command = current_scan.clone();
            continue
        };
        // This is garbage, sorry. Read the input, turn it into u32 that describes the command
        match last_command.clone() {
            _close if _close.action == Action::Close => break,
            audit if audit.action == Action::Audit => {
                println!("{}", audit.data);
                // set the audit info struct with all the info we need (location_id currently not working in this struct)
                let audit_info = snipeit_rust::Audit{
                    asset_tag: current_scan.data,
                    location_id: audit.data.parse::<u32>().unwrap(),
                    next_audit_date: {
                        let now = Local::now();
                        if 3 > now.weekday().num_days_from_monday() as i64 {
                            (now + Duration::days(3 - now.weekday().num_days_from_monday() as i64)).format("%Y-%m-%d").to_string()
                        } else {
                            (now + Duration::days(8 - now.weekday().num_days_from_monday() as i64)).format("%Y-%m-%d").to_string()
                        }
                    }

                };

                // workaround to get the location updated based on the data scanned
                // verify that the asset exist in the code as audit api will always return 200
                let asset = client.get_asset_by_tag(audit_info.asset_tag.as_str());
                let mut asset = match asset {
                    Ok(t) => t,
                    Err(_) => {
                        println!("Asset Not Found");
                        play_bad();
                        continue;
                    }
                }.to_update();
                asset.rtd_location_id = Some(audit_info.location_id);
                match client.update_asset(asset) {
                    Ok(_) => println!("Updated location"),
                    Err(_) => {
                        println!("Update Failed");
                        play_bad();
                        continue;
                    }
                }

                // run the audit api call
                let audit_status = client.audit_asset(audit_info);
                match audit_status {
                    Ok(t) => println!("Audited - {}", t),
                    // error will never run, only returns 200 (Status okay)
                    Err(_) => println!("Error")
                }
                play_good();
            },
            report if report.action == Action::Report => {
                println!("Command Not Implemented");
            },
            new if new.action == Action::New => {
                match new.data.as_str() {
                    "Dell" => {
                        let dell_client = dell_api::DellClient::new("l7b28d74ceecc04ebaab993d68e976da2c", "dbb4a4401aee4e89a21e24447fdd5af6");
                        let dell_client = match dell_client {
                            Ok(dell_client) => {dell_client}
                            Err(_) => {
                                println!("Unable to connect to dell api");
                                continue;
                            }
                        };
                        let machine = dell_client.assets_from_servicetags(&*current_scan.data);
                        let machine = match machine {
                            Ok(machine) => {machine}
                            Err(_) => {
                                println!("unable to find ST");
                                play_bad();
                                continue;
                            }
                        };
                        let warranty = dell_client.assets_warranty_from_servicetags(&*current_scan.data);
                        let warranty = match warranty {
                            Ok(warranty) => {warranty}
                            Err(_) => {
                                println!("unable to find ST");
                                play_bad();
                                continue;
                            }
                        };

                        let machine = &machine[0];
                        let warrantys = &mut warranty[0].entitlements.clone();
                        warrantys.sort_by(|entitlement_one, entitlement_two| entitlement_one.endDate.cmp(&entitlement_two.endDate));
                        let warranty_end_date = &warrantys[warrantys.len() - 1].endDate;
                        let warranty_months = date_component::date_component::calculate(
                            &machine.shipDate.parse::<DateTime::<Utc>>().unwrap(),
                            &warranty_end_date.parse::<DateTime::<Utc>>().unwrap());

                        let warranty_months = warranty_months.month + (warranty_months.year * 12);

                        println!("{}", machine.productLineDescription);
                        let model_id = match machine.productLineDescription.as_str() {
                            "PRECISION 5550" => 28,
                            "LATITUDE 3500" => 93,
                            "PRECISION 7560" => 22,
                            "PRECISION 5560" => 43,
                            "PRECISION 5570" => 44,
                            "LATITUDE 9330 CONVERTIBLE" => 11,
                            "XPS 15 9500" => 29,
                            "PRECISION 7550" => 23,
                            "LATITUDE 5550" => 94,
                            "PRECISION 7520" => 8,
                            "PRECISION 7680" => 81,
                            "LATITUDE 5411" => 78,
                            "LATITUDE 5430" => 74,
                            "LATITUDE 5320" => 90,
                            "PRECISION 7530" => 9,
                            "LATITUDE 5420" => 3,
                            "XPS 13 9360" => 95,
                            "LATITUDE 5511" => 30,
                            "PRECISION 3530" => 42,
                            "PRECISION 7540" => 10,
                            "PRECISION 7670" => 96,
                            _ => 0
                        };

                        if model_id == 0 {
                            play_bad();
                            continue;
                        }

                        let purchase_date = machine.shipDate.split("T").next().unwrap().to_string();

                        let status_id = match &warranty_end_date.parse::<DateTime::<Utc>>().unwrap() > &Utc::now() {
                            true => 4,
                            false => 5
                        };

                        let add_asset = snipeit_rust::AddAsset {
                            asset_tag: get_input(),
                            status_id: status_id,
                            model_id,
                            serial: machine.serviceTag.clone(),
                            purchase_date,
                            warranty_months: warranty_months as u32,
                        };

                        match client.add_asset(add_asset) {
                            Ok(_) => {play_good()}
                            Err(_) => {play_bad()}
                        };
                    },
                    "Microsoft" => {},
                    "Apple" => {},
                    _ => {}
                }
            },
            // if scanning any code that is not z,a, or t prefixed
            _ => println!("Command Not Found"),
        }
    }
}

// literally just a code block to wipe the screen and get some input
fn get_input() -> String {
    print!("\x1B[2J\x1B[1;1H");
    let stdin = io::stdin();
    let mut buffer = String::with_capacity(1024);
    let _line = stdin.read_line(&mut buffer);
    buffer.trim().to_string()
}

// converts the input (or any valid prefixed string, technically) into u32 code to be used
fn read_command(input: String) -> Command {
    let mut command = input.split("-");
    let check = command.next().unwrap();
    let action = if check.len() == 1 {
        let first_char = check.chars().next().unwrap();
        match first_char {
            'z' => Action::Close,
            'a' => Action::Audit,
            'g' => Action::Report,
            'n' => Action::New,
            _ => Action::None
        }
    } else {
        Action::None
    };

    let data = if action == Action::None {
        input.trim().to_string()
    } else {
        command.next().unwrap().trim().to_string()
    };
    Command {
        action,
        data
    }
}

// TODO: Fix this on systems without sound
fn play_good() {
    let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
    let sink = rodio::Sink::try_new(&handle).unwrap();
    sink.set_volume(3.0);

    let file = std::fs::File::open("./assets/Good.mp3").unwrap();
    sink.append(rodio::Decoder::new(BufReader::new(file)).unwrap());

    sink.sleep_until_end();
}

fn play_bad() {
    let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
    let sink = rodio::Sink::try_new(&handle).unwrap();
    sink.set_volume(3.0);

    let file = std::fs::File::open("./assets/NotFound.mp3").unwrap();
    sink.append(rodio::Decoder::new(BufReader::new(file)).unwrap());

    sink.sleep_until_end();
}